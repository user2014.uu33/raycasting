#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include "M_Map.h"

using namespace std;

int screenWidth = 720;
int screenHeight = 512;
sf::Vector2f dir;
#define PI 3.141592

sf::RenderWindow window(sf::VideoMode(screenWidth, screenHeight), "RayCasting");

float pa, px, py, pdx, pdy;
float PlayerSpeed = 0.7f;
float PlayerSizeX = 8.f;
float PlayerSizeY = 8.f;

const float BoxSizeX = 32.f, BoxSizeY = 32.f;
float BoxPosX, BoxPosY, MainTracePosX, MainTracePosY;

float BoxSizeFromPlayerPosX = 0.f;
float BoxSizeFromPlayerPosY = 0.f;

float SizeMainTrace = 96;
int RaysVlaue = 256; 
int BoxValue;

sf::RectangleShape Player(sf::Vector2f(PlayerSizeX, PlayerSizeY));
std::vector<sf::RectangleShape> Traces(RaysVlaue);
sf::RectangleShape BackTrace;
std::vector<sf::RectangleShape> Walls(RaysVlaue);

std::vector<std::vector<sf::RectangleShape>> Box(MapHeight, std::vector<sf::RectangleShape>(MapWidth));
std::vector<std::vector<sf::RectangleShape>> Floor(MapHeight, std::vector<sf::RectangleShape>(MapWidth));

std::vector<float> DistRays(RaysVlaue);
float resDistRays;

void Controls()
{
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
    {
        for (int i = 0; i < RaysVlaue; ++i)
        {
            Traces[i].rotate(-2.f);
        }
        px = sin(Traces[RaysVlaue / 2].getRotation() * PI / 180) * -3;
        py = cos(Traces[RaysVlaue / 2].getRotation() * PI / 180) * 3;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
    {
        for (int i = 0; i < RaysVlaue; ++i)
        {
            Traces[i].rotate(2.f);
        }
        px = sin(Traces[RaysVlaue / 2].getRotation() * PI / 180) * -3;
        py = cos(Traces[RaysVlaue / 2].getRotation() * PI / 180) * 3;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
    {
        pdx = PlayerSpeed * px;
        pdy = PlayerSpeed * py;
        Player.move(pdx, pdy);
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
    {
        pdx = PlayerSpeed * px;
        pdy = PlayerSpeed * py;
        Player.move(-pdx, -pdy);
    }
    dir = { px, py };
}

void DrawMap()
{
    for (int i = 0; i < MapWidth; ++i)
    {
        for (int j = 0; j < MapHeight; ++j)
        {
            if (WorldMap[i][j] == 1)
            {
                Box[i][j].setSize(sf::Vector2f(BoxSizeX, BoxSizeY));
                Box[i][j].setPosition(sf::Vector2f(i * BoxSizeX, j * BoxSizeY));
                Box[i][j].setFillColor(sf::Color::Red);
                window.draw(Box[i][j]);
                BoxValue++;
                if (WorldMap[int(Player.getPosition().x) / 32][int(Player.getPosition().y) / 32] != 0)
                    PlayerSpeed = 0;
            }
            else if (WorldMap[i][j] == 0)
            {
                Floor[i][j].setSize(sf::Vector2f(BoxSizeX, BoxSizeY));
                Floor[i][j].setPosition(sf::Vector2f(i * BoxSizeX, j * BoxSizeY));
                Floor[i][j].setFillColor(sf::Color::Blue);
                Floor[i][j].setOutlineThickness(1);
                Floor[i][j].setOutlineColor(sf::Color::White);
                window.draw(Floor[i][j]);
            }
        }
    }
}

void CheckIsPlayerNearWall(float DistToWall)
{
    if (DistToWall <= 32)
        PlayerSpeed = 0;
    else
        PlayerSpeed = 0.5f;
}


void RayCast()
{
    for (int j = 0; j < RaysVlaue; ++j)
    {
        float RayIterator = 0.f, c = 0.0f;
        sf::Vector2f EndTracePos;
        for (; RayIterator < SizeMainTrace; RayIterator += .05)
        {   
            float px = sin(Traces[j].getRotation() * PI / 180) * -3;
            float py = cos(Traces[j].getRotation() * PI / 180) * 3;

            float EndTraceX = RayIterator * px;
            float EndTraceY = RayIterator * py;
            
            EndTracePos = { Traces[j].getPosition().x + EndTraceX, Traces[j].getPosition().y + EndTraceY };

            c = sqrt(pow(EndTraceX, 2) + pow(EndTraceY, 2));
            DistRays[j] = c;

            if (WorldMap[int(EndTracePos.x) / 32][int(EndTracePos.y) / 32] != 0)
                break;
        }

        Traces[j].setPosition(Player.getPosition().x + (PlayerSizeX / 2), Player.getPosition().y + (PlayerSizeX / 2));
        Traces[j].setSize(sf::Vector2f(2.f, DistRays[j]));
        window.draw(Traces[j]);

        //Draw 3d
        float SizeY = 80;
        int PosX = screenWidth / 3;
        
        float ChangeSizeY = (DistRays[j] / SizeY) * 100;
        float SizeYWall = SizeY - ChangeSizeY + 200;
        Walls[j].setSize(sf::Vector2f(3, int(SizeYWall)));
        Walls[j].setPosition((screenWidth - PosX - 100) + (j * 3) - (screenWidth / 2) - 20, float(screenHeight - SizeY + ChangeSizeY) / 2);
        if (SizeYWall > 250)
            SizeYWall = 250;
        Walls[j].setFillColor(sf::Color(255, SizeYWall, SizeYWall));
        window.draw(Walls[j]);
    }
    CheckIsPlayerNearWall(DistRays[RaysVlaue / 2]);
}

int main()
{
    const sf::Time TimePerFrame = sf::seconds(1.f / 120.f);
    sf::RectangleShape BackGround, Floor;
    BackGround.setPosition(0, 0);
    BackGround.setSize(sf::Vector2f(screenWidth, screenHeight));
    BackGround.setFillColor(sf::Color(135, 206, 235));

    Floor.setPosition(0, 355);
    Floor.setSize(sf::Vector2f(screenWidth, screenHeight - 350));
    Floor.setFillColor(sf::Color(128, 128, 128));
    
    Player.setPosition(64, 64);
    Player.setFillColor(sf::Color::Red);
    
    sf::Clock clock;

    for (int i = 0; i < RaysVlaue; ++i)
    {
        Traces[i].setSize(sf::Vector2f(1.f, 1.f));
        Traces[i].setOrigin(0.1f, 0.1f);
        Traces[i].setFillColor(sf::Color::Green);
        Traces[i].setRotation(-30 + i * 0.3);
    }

    while (window.isOpen())
    {
        sf::Time elapsed = clock.restart();
        sf::Event event;
        while (window.pollEvent(event))
            if (event.type == sf::Event::Closed) window.close();

        window.draw(BackGround);
        window.draw(Floor);
        Controls();
        DrawMap();
        RayCast();
        window.draw(Player);
        window.display();
    }
  
    return 0;
}