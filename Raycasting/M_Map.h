#pragma once
#include <vector>

const int MapHeight = 8;
const int MapWidth = 8;
int WorldMap[MapWidth][MapHeight]
{
    1,1,1,1,1,1,1,1,
    1,0,0,0,0,0,0,1,
    1,0,0,0,0,0,0,1,
    1,0,0,1,0,0,0,1,
    1,0,0,0,0,1,0,1,
    1,0,0,1,0,1,0,1,
    1,0,0,0,0,0,0,1,
    1,1,1,1,1,1,1,1,
};
	